# MI PDF Generator

Der PDF Generator ist eine Clientseitige Anwendung und stellt verschiedene Templates bereit, bei denen ausschließlich die benötigten Informationen eingetragen werden müssen. Daraufhin kann die PDF-Datei heruntergeladen werden.

## Start

```bash
git clone https://gitlab.com/dplettlinger/mi-pdf-generator.git
```
```bash
git clone https://gitlab.com/dplettlinger/mi-pdf-generator.git
```
or download

## Open

index.html

## Screenshot

![Startscreen](/img/screenshots/mi-pdf-generator-screenshot.png "Image Title")

<!-- blank line -->
----
<!-- blank line -->

![Input section](/img/screenshots/mi-pdf-generator-input-screenshot.png "Image Title")
