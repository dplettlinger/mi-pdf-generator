function sdPdfSetting(){

    var sd = new jsPDF ({
       orientation: "p",
       unit: "mm",
       format: "a4"
    });

    var heading = document.getElementById('heading').value;
    var subheading = document.getElementById('subheading').value;
    var time = document.getElementById('time').value;
    var text = document.getElementById('text').value;
    var filename = document.getElementById('filename').value;

    sd.addImage(sdImgData, 'png', 0, 80, 200, 130);
    sd.addImage(logoImgData, 'png', (210-35), (297-20), 30, 15);

    sd.setFontSize(40);
    var splitHeading = sd.splitTextToSize(heading, 150);
    sd.text(10, 30, splitHeading);

    sd.setFontSize(16);
    sd.setFontStyle('bold');
    sd.text(10, 220, time);

    sd.setTextColor(100);
    sd.setFontSize(14);
    sd.text(10, 230, subheading);

    sd.setTextColor(100);
    sd.setFontSize(12);
    var splitText = sd.splitTextToSize(text, 180);
    sd.text(10, 240, splitText);

    // Output as Data URI
    sd.output('datauri');

    sd.save(filename + '.pdf');
}
