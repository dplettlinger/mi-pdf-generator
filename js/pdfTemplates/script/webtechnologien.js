function wtwPdfSetting(){

    var wtw = new jsPDF ( {
       orientation: "p",
       unit: "mm",
       format: "a4"
    });

    var heading = document.getElementById('heading').value;
    var subheading = document.getElementById('subheading').value;
    var time = document.getElementById('time').value;
    var text = document.getElementById('text').value;
    var filename = document.getElementById('filename').value;

    wtw.addImage(wtwImgData, 'png', 0, 0, 200, 130);
    wtw.addImage(logoImgData, 'png', (210-35), (297-20), 30, 15);

    wtw.setFontSize(27);
    var splitHeading = wtw.splitTextToSize(heading, 150);
    wtw.text(10, 140, splitHeading);

    wtw.setFontSize(16);
    wtw.setFontStyle('bold');
    wtw.text(10, 160, time);

    wtw.setTextColor(100);
    wtw.setFontSize(14);
    wtw.text(10, 170, subheading);

    wtw.setTextColor(100);
    wtw.setFontSize(12);
    var splitText = wtw.splitTextToSize(text, 180);
    wtw.text(10, 180, splitText);

    // Output as Data URI
    wtw.output('datauri');

    wtw.save(filename + '.pdf');
}
