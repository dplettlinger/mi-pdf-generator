var selectedPdfTemplate = document.getElementById('selectedTemplate');

$(".pdf-template").click(function(){

  var pdfTemplateId = $(this).attr("id");
  $("#overlay-section").addClass('active');
  $(".app-name").text("Go Back");

  $('.pdf-designs-section').addClass('hidden-overflow');
  $('.app-name').addClass('back-to-site-button');

  var getPdfId = document.getElementById(pdfTemplateId);
  var imageSrc = getPdfId.getElementsByTagName('img')[0].src;

  var pdfImage = document.createElement ('img');
  pdfImage.src = imageSrc;
  pdfImage.className = 'selected-template';
  pdfImage.id = pdfTemplateId;
  pdfImage.alt = 'template-pic';
  selectedPdfTemplate.appendChild(pdfImage);
});

$(".app-name").click(function(){
    $('.pdf-designs-section').removeClass('hidden-overflow');
    $('.app-name').removeClass('back-to-site-button');

    var images = document.getElementsByClassName('selected-template');
    var l = images.length;
    for (var i = 0; i < l; i++) {
        images[0].parentNode.removeChild(images[0]);
    }

    var overlayElement = document.getElementById('overlay-section')
    overlayElement.classList.remove("active");
    $(".app-name").text('PDF Generator')
})
